#!/usr/bin/env sh

pip install -r requirements-dev.txt
python -c "import nltk; \
    nltk.download('punkt'); \
    nltk.download('averaged_perceptron_tagger'); \
    nltk.download('maxent_ne_chunker'); \
    nltk.download('words'); \
    nltk.download('wordnet');"
